// server.js
const express = require('express');
const path = require('path');

const app = express();
const PORT = process.env.PORT || 3000;

// Serve static files from the 'dist' directory
app.use(express.static(path.join(__dirname, 'dist')));

// Define routes
const imprint = 'imprint'; // keep this same as /src/pages/<file name>
const error = 'error'; // keep this same as /src/pages/<file name>
const siteCredits = 'site-credits'; // keep this same as /src/pages/<file name>
const community = 'community'; // keep this same as /src/pages/<file name>
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist', 'index.html'));
});

app.get(`/${imprint}`, (req, res) => {
  res.sendFile(path.join(__dirname, `dist/${imprint}`, 'index.html'));
});
app.get(`/${siteCredits}`, (req, res) => {
  res.sendFile(path.join(__dirname, `dist/${siteCredits}`, 'index.html'));
});

app.get(`/${community}`, (req, res) => {
  res.sendFile(path.join(__dirname, `dist/${community}`, 'index.html'));
});

// Set up a 404 page
app.use((req, res, next) => {
  res.status(404).sendFile(path.join(__dirname, `dist/${error}`, 'index.html'));
});

// Start the server
app.listen(PORT, () => {
  console.log(`http://localhost:${PORT}`);
});
