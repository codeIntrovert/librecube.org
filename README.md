# Librecube Space Research Community
Welcome to the Librecube Space Research Community! This is the central hub for open-source enthusiasts, researchers, and space exploration aficionados. Librecube aims to foster collaboration, innovation, and knowledge exchange in the exciting field of space research.

## About
Librecube is a community-driven initiative dedicated to advancing space exploration through open collaboration and sharing of resources. Our community comprises scientists, engineers, students, and enthusiasts passionate about exploring the cosmos and pushing the boundaries of space technology.

## Live Link

https://librecube-org.vercel.app/

## Features


- Community Forum: Engage with like-minded individuals, ask questions, share ideas, and collaborate on projects in our vibrant online forum.


- Project Repository: Explore a diverse range of open-source projects related to space research, from satellite development to astrophysics simulations.


- Events Calendar: Stay updated on upcoming space-related events, webinars, workshops, and conferences.


- Resource Library: Access a wealth of educational materials, research papers, and tutorials to expand your knowledge of space science and technology.

## 🚀 Project Structure

Inside of your Astro project, you'll see the following folders and files:

```
/
├── public/
│   └── favicon.svg
├── src/
│   ├── components/
│   │   ├── AppHeader.astro
│   │   ├── AppFooter.astro
│   │   ├── Features.astro
│   │   ├── HeroSection.astro
│   │   └── Blog.astro
│   ├── layouts/
│   │   └── Layout.astro
│   └── pages/
│       ├── imprint.astro
│       └── index.astro
├── server.js
├── README.md
├── astro.config.mjs
└── package.json
```

Astro looks for `.astro` or `.md` files in the `src/pages/` directory. Each page is exposed as a route based on its file name.

There's nothing special about `src/components/`, but that's where we like to put any Astro/React/Vue/Svelte/Preact components.

Any static assets, like images, can be placed in the `public/` directory.

## 🧞 Commands

All commands are run from the root of the project, from a terminal:

| Command                | Action                                             |
| :--------------------- | :------------------------------------------------- |
| `npm install`          | Installs dependencies                              |
| `npm run dev`          | Starts local dev server at `localhost:3000`        |
| `npm run build`        | Build your production site to `./dist/`            |
| `node server.js`       | Starts production & serves `./dist/` files         |

## 👀 Want to learn more?

Feel free to check [our documentation](https://docs.astro.build) or jump into our [Discord server](https://astro.build/chat).
